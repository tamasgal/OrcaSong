"""
For backwards compatibility.
"""
import warnings
from orcasong.core import FileBinner

# TODO deprecated
warnings.warn("orcasong_2 has been renamed to orcasong, please update your code.")
